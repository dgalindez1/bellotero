## Table of Contents

- [Getting Started](#getting-started)
- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm run build](#npm-run-build)
- [Third Party Modules](#third-party-modules)

## Getting Started

In the project directory, run:

### `npm install`

Once susccessful, run:

### `npm start`

To see the local server.

You can also run:

### `npm run build`

To build a deployable version.

## Folder Structure

```
bellotero/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
    manifest.json
  scripts/
    start.js
    build.js
  config/
    env.js
    paths.js
    polyfills.js
    webpack.config.dev.js 
    webpack.config.prod.js
    webpackDevServer.config.js
    jest/
      cssTransform.js
      fileTransform.js
  src/
    App.less
    App.js
    index.js
    registerServiceWorker.js
    Header/
      Header.js
      Header.less
      images/
    Bellotero/
      Bellotero.js
      Calculator/
        Calculator.js
        Calculator.less
      Carousel/
        Carousel.js
        Carousel.less
        CarouselItem/
          CarouselItem.js
          CarouselItem.less
          images/
      EmailBottom/
        EmailBottom.js
        EmailBottom.less
      EmailField/
        EmailField.js
      EmailTop/
        EmailTop.js
        EmailTop.less
        images/
      Introduction/
        Introduction.js
        Introduction.less
        images/
        LogoBar/
          LogoBar.js
          LogoBar.less
          images/
      Statistics/
        Statistics.js
        Statistics.less
        images/
    Footer/
      Footer.js
      Footer.less
      images/
```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Third Party Modules

### `create-react-app`

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### `reactstrap`

This project includes Bootstrap via [Reactstrap](https://github.com/reactstrap/reactstrap).

### `react-slick`

This project uses the react version of slick carousel with [React Slick](https://github.com/akiran/react-slick).

### `react-input-range`

This project uses an input range slider component, [React Input Range](https://github.com/davidchin/react-input-range).
