import React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button
} from 'reactstrap';

// CSS
import './Header.less';

// Images
import Logo from './images/bellotero-logo.svg';

const Header = () => (
  <div className="header">
    <Navbar expand="sm">
      <NavbarBrand>
        <img src={Logo} alt="Bellotero.io Logo"/>
      </NavbarBrand>
      <Nav navbar className="ml-auto">
        <NavItem>
          <NavLink href='#'>Features</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href='#'>Solutions</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href='#'>Stories</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href='#'>Partners</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href='#'>About</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href='#'>Blog</NavLink>
        </NavItem>
        <NavItem>
          <Button className="demo">Request a demo</Button>
        </NavItem>
        <NavItem>
          <Button className="login">Log In</Button>
        </NavItem>
      </Nav>
    </Navbar>
  </div>
);

export default Header;
