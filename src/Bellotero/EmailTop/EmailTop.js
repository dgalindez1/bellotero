import React from 'react';

// Components
import EmailField from '../EmailField/EmailField';

// CSS
import './EmailTop.less';

// Images
import HeroImage from './images/hero-mockup.png';
import HeroImage2x from './images/hero-mockup@2x.png';
import HeroImage3x from './images/hero-mockup@3x.png';

const EmailTop = () => (
  <div className="top">
    <div className="text">
      <strong>Digitize your invoices</strong>
      <span> and create your own shopping cart.</span>
    </div>
    <EmailField />
    <img
      src={HeroImage}
      srcSet={`${HeroImage2x} 2x, ${HeroImage3x} 3x`}
      alt="Bellotero in Phone, Tablet and Laptop"
    />
  </div>
);

export default EmailTop;
