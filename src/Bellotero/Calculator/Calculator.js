import React, { Component } from 'react';
import {
  Label,
  Input,
  InputGroup,
} from 'reactstrap';
import InputRange from 'react-input-range';

// CSS
import './Calculator.less';

const FOOD_SAVINGS_FACTOR = 0.3;
const ANNUAL_SAVINGS_FACTOR = 1337;
const MIN_MONTHLY_SPENDING = 10.0;
const MAX_MONTHLY_SPENDING = 100.0;
const MIN_FULL_TIME_EMPLOYEES = 1;
const MAX_FULL_TIME_EMPLOYEES = 10;
const DECIMALS_SHOWN = 2;

class Calculator extends Component  {
  constructor(props) {
    super(props);

    this.state = {
      monthlySpending: 10.00,
      fullTimeEmployees: 1,
      foodSavings: 3.00,
      annualSavings: 1340.00
    };

    this.calculateEstimates = this.calculateEstimates.bind(this);
    this.updateMonthlySpending = this.updateMonthlySpending.bind(this);
    this.updateFullTimeEmployees = this.updateFullTimeEmployees.bind(this);
  }

  // Due to JS precision loss, this will round values such as 1.005 to 1 instead of 1.01
  // For UX it seems to be an acceptable situation, since this avoids the user seeing values
  // with more than 2 decimal places for a monetary value
  roundDecimals(number) {
    return parseFloat((Math.round(number * 100) / 100).toFixed(DECIMALS_SHOWN));
  }

  calculateEstimates() {
    const foodSavings = this.roundDecimals(this.state.monthlySpending * FOOD_SAVINGS_FACTOR);
    const annualSavings = this.roundDecimals(this.state.fullTimeEmployees * ANNUAL_SAVINGS_FACTOR + foodSavings);
    this.setState({foodSavings, annualSavings});
  }
  
  updateMonthlySpending(value) {
    let monthlySpending = this.roundDecimals(value);
    this.setState({ monthlySpending });
    this.calculateEstimates();
  }
  
  updateFullTimeEmployees(fullTimeEmployees) {
    this.setState({ fullTimeEmployees });
    this.calculateEstimates();
  }

  render() {
    return (
      <div className="calculator">
        <p className="title">See how much you can save with Bellotero.io</p>
        <p className="description">With Bellotero.io you save time and money make real-time decisions that boost your business and your bottom line. Get less wrongfully blocked payments, save time on bookkeeping and no need to worry about safety.</p>
        <InputGroup>
          <Label for="monthlySpending">Monthly ingredient spending</Label>
          <Input 
            type="text"
            name="monthlySpending"
            value={`$${this.state.monthlySpending}`}
            disabled
          />
          <InputRange
            minValue={MIN_MONTHLY_SPENDING}
            maxValue={MAX_MONTHLY_SPENDING}
            formatLabel={() => ''}
            step={0.01}
            value={this.state.monthlySpending}
            onChange={this.updateMonthlySpending}
          />
        </InputGroup>
        <InputGroup>
          <Label for="fullTimeEmployees">Full-time employees that process invoices</Label>
          <Input 
            type="text"
            name="fullTimeEmployees"
            value={this.state.fullTimeEmployees}
            disabled
          />
          <InputRange
            minValue={MIN_FULL_TIME_EMPLOYEES}
            maxValue={MAX_FULL_TIME_EMPLOYEES}
            formatLabel={() => ''}
            value={this.state.fullTimeEmployees}
            onChange={this.updateFullTimeEmployees}
          />
        </InputGroup>
        <div className="annual-savings">
          <p className="title">Your estimated annual savings</p>
          <p className="value">
            <span className="unit">$</span>
            {this.state.annualSavings.toFixed(DECIMALS_SHOWN)}
          </p>
        </div>
        <div className="food-savings">
          <p className="title">Estimated food cost savings</p>
          <p className="value">
            <span className="unit">$</span>
            {this.state.foodSavings.toFixed(DECIMALS_SHOWN)}
          </p>
        </div>
      </div>
    );
  }
}

export default Calculator;
