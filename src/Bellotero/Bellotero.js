import React from 'react';

import EmailTop from './EmailTop/EmailTop';
import Introduction from './Introduction/Introduction';
import Statistics from './Statistics/Statistics';
import Carousel from './Carousel/Carousel';
import Calculator from './Calculator/Calculator';
import EmailBottom from './EmailBottom/EmailBottom';

const Bellotero = () => (
  <div className="bellotero">
    <EmailTop />
    <Introduction />
    <Statistics />
    <Carousel />
    <Calculator />
    <EmailBottom/>
  </div>
);

export default Bellotero;
