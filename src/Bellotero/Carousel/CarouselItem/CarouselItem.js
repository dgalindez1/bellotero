import React from 'react';

// CSS
import './CarouselItem.less';

// Images
import QuotationMark from './images/quotation-marks.svg';

const CarouselItem = (props) => (
  <div>
    <img src={QuotationMark}  alt="Quotation Mark" className="quotation-mark" />
    <div className="body">
      <div className="quote">{props.quote}</div>
      <div className="author">{props.author}</div>
      <div className="detail">{props.detail}</div>
    </div>
</div>
);

export default CarouselItem;
