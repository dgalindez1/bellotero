import React from 'react';
import Slider from 'react-slick';

import CarouselItem from './CarouselItem/CarouselItem';

// CSS
import './Carousel.less';

const settings = {
  dots: true,
  arrows: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
};

const data = [
  {
    quote: 'It\'s funny what memory does, isn\'t it? My favorite holiday tradition might not have happened more than once or twice. But because it is such a good memory, so encapsulating of everything I love about the holidays, in my mind it happened every year. Without fail.',
    author: 'Molly O’Keefe',
    detail: 'American Author'
  },
  {
    quote: 'When nine hundred years old you reach, look as good you will not.',
    author: 'Yoda',
    detail: 'Jedi Master'
  },
  {
    quote: 'The saddest aspect of life right now is that science gathers knowledge faster than society gathers wisdom.',
    author: 'Isaac Asimov',
    detail: 'Russian Author'
  },
  {
    quote: 'Do you wish me a good morning, or mean that it is a good morning whether I want it or not; or that you feel good this morning; or that it is a morning to be good on?',
    author: 'Gandalf',
    detail: 'Grey Wizard'
  },
  {
    quote: 'One does not simply walk into mordor',
    author: 'Boromir',
    detail: 'Son of Denethor'
  },
];

const Carousel = () => (
  <div className="carousel-container">
    <span className="title">Our customers love us</span>
    <Slider {...settings}>
      {data.map((item, index) => <CarouselItem key={`carousel-${index}`} {...item} />)}
    </Slider>
  </div>
);

export default Carousel;
