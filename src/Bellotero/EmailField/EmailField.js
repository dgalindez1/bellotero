import React from 'react';
import {
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';

// TODO: add click to addon

const EmailField= () => (
  <InputGroup className="email">
    <Input
      type="email"
      name="email"
      id="email"
      placeholder="Your email address"
    />
    <InputGroupAddon addonType="append">Request a demo</InputGroupAddon>
  </InputGroup>
);

export default EmailField;
