import React from 'react';
import { Container, Row, Col } from 'reactstrap';

// CSS
import './LogoBar.less';

// Images
import Logo1 from './images/logo-1.png';
import Logo1_2x from './images/logo-1@2x.png';
import Logo1_3x from './images/logo-1@3x.png';

import Logo2 from './images/logo-2.png';
import Logo2_2x from './images/logo-2@2x.png';
import Logo2_3x from './images/logo-2@3x.png';

import Logo3 from './images/logo-3.png';
import Logo3_2x from './images/logo-3@2x.png';
import Logo3_3x from './images/logo-3@3x.png';

import Logo4 from './images/logo-4.png';
import Logo4_2x from './images/logo-4@2x.png';
import Logo4_3x from './images/logo-4@3x.png';

import Logo5 from './images/logo-5.png';
import Logo5_2x from './images/logo-5@2x.png';
import Logo5_3x from './images/logo-5@3x.png';

import Logo6 from './images/logo-6.png';
import Logo6_2x from './images/logo-6@2x.png';
import Logo6_3x from './images/logo-6@3x.png';

const LogoBar = () => (
  <div className="logo-bar">
    <Container>
      <Row>
        <Col>
          <img
            src={Logo1}
            srcSet={`${Logo1_2x} 2x, ${Logo1_3x} 3x`}
            alt="Logo 1"
          />
        </Col>
        <Col>
          <img
            src={Logo2}
            srcSet={`${Logo2_2x} 2x, ${Logo2_3x} 3x`}
            alt="Logo 2"
          />
        </Col>
        <Col>
          <img
            src={Logo3}
            srcSet={`${Logo3_2x} 2x, ${Logo3_3x} 3x`}
            alt="Logo 3"
          />
        </Col>
        <Col>
          <img
            src={Logo4}
            srcSet={`${Logo4_2x} 2x, ${Logo4_3x} 3x`}
            alt="Logo 4"
          />
        </Col>
        <Col>
          <img
            src={Logo5}
            srcSet={`${Logo5_2x} 2x, ${Logo5_3x} 3x`}
            alt="Logo 5"
          />
        </Col>
        <Col>
          <img
            src={Logo6}
            srcSet={`${Logo6_2x} 2x, ${Logo6_3x} 3x`}
            alt="Logo 6"
          />
        </Col>
      </Row>
    </Container>
  </div>
);

export default LogoBar;
