import React from 'react';

// Components
import LogoBar from './LogoBar/LogoBar';

// CSS
import './Introduction.less';

// Images
import ArrowRight from './images/arrow-right.svg';

import IntroImage1 from './images/intro-img-1.png';
import IntroImage1_2x from './images/intro-img-1@2x.png';
import IntroImage1_3x from './images/intro-img-1@3x.png';

import IntroImage2 from './images/intro-img-2.png';
import IntroImage2_2x from './images/intro-img-2@2x.png';
import IntroImage2_3x from './images/intro-img-2@3x.png';

// TODO: Make Learn more's clickable

const Introduction = () => (
  <div className="intro">
    <LogoBar />
    <div className="info">
      <p>Bellotero.io is the digital solution that gives you fast, accurate, automated accounts payable and smart, business-transforming insights.</p>
      <img
        src={IntroImage1}
        srcSet={`${IntroImage1_2x} 2x, ${IntroImage1_3x} 3x`}
        alt="Bellotero in tablet and phone"
        className="intro-image"
      />
      <p className="title threads-text">
        Get the full picture.<br />
        In half the time.
      </p>
      <p className="body threads-text">Threads keep all your conversations clearly separated by topic so replies won’t get buried in an endless stream of group chat. </p>
      <div className="learn-more">
        Learn more
        <img src={ArrowRight}  alt="Arrow right" />
      </div>
      <img
        src={IntroImage2}
        srcSet={`${IntroImage2_2x} 2x, ${IntroImage2_3x} 3x`}
        alt="Bellotero in laptop"
        className="intro-image-2"
      />
      <p className="title topic-text">Timesaving, moneymaking.</p>
      <p className="body topic-text">Bellotero.io automatically turns your threaded conversations into a searchable catalog of topics.</p>
      <div className="learn-more second">
        Learn more
        <img src={ArrowRight}  alt="Arrow right" />
      </div>
    </div>
  </div>
);

export default Introduction;
