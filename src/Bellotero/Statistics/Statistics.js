import React from 'react';

// CSS
import './Statistics.less';

// Images
import Photo1 from './images/photo-1.png';
import Photo1_2x from './images/photo-1@2x.png';
import Photo1_3x from './images/photo-1@3x.png';

import Photo2 from './images/photo-2.png';
import Photo2_2x from './images/photo-2@2x.png';
import Photo2_3x from './images/photo-2@3x.png';

import Photo3 from './images/photo-3.png';
import Photo3_2x from './images/photo-3@2x.png';
import Photo3_3x from './images/photo-3@3x.png';

const Statistics = () => (
  <div className="statistics">
    <div>
      <img
        src={Photo1}
        srcSet={`${Photo1_2x} 2x, ${Photo1_3x} 3x`}
        alt="Promotional"
      />
      <div className="image-text">
        <span className="image-title">
          50
          <span className="image-unit">%</span>
        </span>
        <span className="image-body">SAVED IN BOOKKEEPING COSTS</span>
      </div>
    </div>
    <div>
      <img
        src={Photo2}
        srcSet={`${Photo2_2x} 2x, ${Photo2_3x} 3x`}
        alt="Promotional"
      />
      <div className="image-text">
        <span className="image-title">
          100
          <span className="image-unit">h</span>
        </span>
        <span className="image-body">AND MORE SAVED IN BOOKKEEPING TIME</span>
      </div>
    </div>
    <div>
      <img
        src={Photo3}
        srcSet={`${Photo3_2x} 2x, ${Photo3_3x} 3x`}
        alt="Promotional"
      />
      <div className="image-text final">
        <span className="image-title">
          25
          <span className="image-unit">%</span>
        </span>
        <span className="image-body">DECREASE IN FOOD COSTS</span>
      </div>
    </div>
  </div>
);

export default Statistics;
