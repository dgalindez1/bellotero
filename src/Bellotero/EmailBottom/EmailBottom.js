import React from 'react';

// Components
import EmailField from '../EmailField/EmailField';

// CSS
import './EmailBottom.less';

const EmailBottom = () => (
	<div className="bottom">
		<div className="text">
      Ready to get started with Bellotero.<span>io</span>?
		</div>
    <div className="data-text">
      No more manual data entry. Hands off. Thumbs up.
    </div>
    <EmailField />
	</div>
);

export default EmailBottom;
