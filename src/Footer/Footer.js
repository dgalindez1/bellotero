import React from 'react';
import { Container, Row, Col } from 'reactstrap';

// CSS
import './Footer.less';

// Images
import Logo from './images/bellotero-logo-white.svg';

import AppStoreImage from './images/app-store.png';
import AppStoreImage2x from './images/app-store@2x.png';
import AppStoreImage3x from './images/app-store@3x.png';

import GooglePlayImage from './images/google-play.png';
import GooglePlayImage2x from './images/google-play@2x.png';
import GooglePlayImage3x from './images/google-play@3x.png';

// TODO: Link Social media
/* eslint-disable jsx-a11y/href-no-hash */
const Footer = () => (
  <div className="footer">
    <Container>
      <Row className="headings">
        <Col>
          <img src={Logo} alt="Bellotero.io Logo"/>
        </Col>
        <Col>Bellotero.io</Col>
        <Col>Social</Col>
        <Col>Support</Col>
      </Row>
      <Row>
        <Col></Col>
        <Col><a href="#">Features</a></Col>
        <Col><a href="#">Facebook</a></Col>
        <Col>support@bellotero.com</Col>
      </Row>
      <Row>
        <Col></Col>
        <Col><a href="#">Solutions</a></Col>
        <Col><a href="#">Twitter</a></Col>
        <Col>(555) 555-5555</Col>
      </Row>
      <Row>
        <Col></Col>
        <Col><a href="#">Stories</a></Col>
        <Col><a href="#">LinkedIn</a></Col>
        <Col><a href="#">Chat now</a></Col>
      </Row>
      <Row>
        <Col></Col>
        <Col><a href="#">About</a></Col>
        <Col><a href="#">Instagram</a></Col>
        <Col></Col>
      </Row>
      <Row>
        <Col></Col>
        <Col><a href="#">Blog</a></Col>
        <Col></Col>
        <Col className="mobile-stores">
          <img
            src={AppStoreImage}
            srcSet={`${AppStoreImage2x} 2x, ${AppStoreImage3x} 3x`}
            alt="Apple App Store"
          />
        </Col>
      </Row>
      <Row>
        <Col></Col>
        <Col></Col>
        <Col></Col>
        <Col>
          <img
            src={GooglePlayImage}
            srcSet={`${GooglePlayImage2x} 2x, ${GooglePlayImage3x} 3x`}
            alt="Google Play Store"
          />
        </Col>
      </Row>
    </Container>
    <div className="last">
      <Row>
        {/* TODO: Play with offsets/CSS to fix how this looks */}
        <Col>© 1909 Bellotero.io</Col>
        <Col>
          <span>Privacy Policy</span>
          <span>Terms of Service</span>
        </Col>
      </Row>
    </div>
  </div>
);

export default Footer;
