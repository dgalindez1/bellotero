import React from 'react';

// Components
import Header from './Header/Header';
import Bellotero from './Bellotero/Bellotero';
import Footer from './Footer/Footer';

// CSS
import './App.less';

const App = () => (
  <div className="main">
    <Header />
    <Bellotero />
    <Footer />
  </div>
);

export default App;
